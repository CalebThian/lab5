#include "HugeInt.h"
#include <cmath>//log10,log is natural log
#include <iostream>
#include <iomanip>//setw but didn't use
#include <cstdio>//sprintf(), sscanf()
#include <string>//c_str()
#include <cstring>//strcpy()
#include <vector>//vector
#include <algorithm>//reverse()
using namespace std;

HugeInt::HugeInt(int a)
{
	/*int b;
	if(a<0)
		b=-a;
	else
		b=a;*/
	char lNum[100];
	sprintf(lNum,"%d",a);//convert a into a char array
	int s=log10(a);//get digit of a
	for(int r=0;r<s;++r)
	{
		//Here is to save the int into a vector,eg.123456 = (1,2,3,4,5,6) 

		char no[2];
		no[0]=lNum[r];//get one digit each time
		int add;
		sscanf(no,"%d",&add);//convert the char digit into int
		num.push_back(add);//add the int into the vector
	}
	/*if(a<0)
		num.at(0)=-(num.at(0));*/
}


HugeInt::HugeInt()
{
	num.push_back(0);//initialize num to 0 if not giving any parameter
}

HugeInt::HugeInt(string b)
{
	int l=b.length();//get the length to be the size of char array
	char all[l+1];//plus 1 more size , as 12345 its l will be 5
	strcpy(all,b.c_str());//strcpy make a string copy and convert into char array
	/*if()
	{
		for(int r=0;r<l-1;++r)
			all[r]=all[r+1];
		l-=1;
	}*/
	for(int r=0;r<l;++r)
	{
		//same as when int condition
		char no[2];
		no[0]=all[r];
		int add;
		sscanf(no,"%d",&add);
		num.push_back(add);
	}
	/*if(l!=b.length())
		num.at(0)=-(num.at(0));*/
}

vector <int>& HugeInt::getNum()//return & as it might need to change the num
{
	vector <int>& s=num;
	/*s.clear();
	for(int i=0;i<num.size();++i)
		s.push_back(num.at(i));*/
	return s;
	//return num;
}

vector <int> HugeInt::getNum() const//return value as it is const
{
	vector <int> s=num;
	/*s.clear();
	for(int i=0;i<num.size();++i)
		s.push_back(num.at(i));*/
	return s;
}


void HugeInt::setInt(vector <int> s)
{
	//must use push_back but not = as = is something like pointer,it doesn't
	//really save the value into num
	num.clear();
	int si=s.size();
	for(int i=0;i<si;++i)
		num.push_back(s.at(i));
}

ostream& operator <<(ostream& out,HugeInt& number)
{
	int s=number.getNum().size();
	for (int r=0;r<s;r++)
	{
		out<<number.getNum().at(r);//print out the char array digit by digit
	}
	return out;
	/*int i=number.getNum();
	char pNum[100];
	sprintf(pNum,"%d",i);
	out<<pNum;
	return out;*/
}

istream& operator >>(istream& in,HugeInt& number)
{
	int i;
	string sNum;
	in>>sNum;//as we don't know how long the int is, read it by string 
	//the below same as the constructor of read-a-string
	int l=sNum.length();
	char iNum[l+1];
	strcpy(iNum,sNum.c_str());
	vector <int> integer;
	for(int r=0;r<l;++r)
	{
		char no[2];
		no[0]=iNum[r];
		int add;
		sscanf(no,"%d",&add);
		integer.push_back(add);
    }
	number.setInt(integer);//only here might be different, but still logic
	return in;
	/*char iNum[100];
	int i;
	in>>iNum;
	sscanf(iNum,"%d",&i);
	number.setInt(i);	
	return in;*/
}

HugeInt HugeInt::operator+(HugeInt& number)
{
	HugeInt add;
	vector <int> tempo1,tempo2,addR;//addR is the reverse of final ans
	int s1=num.size();
	int s2=number.getNum().size();
		
	//tempo1=num;
	for(int i=0;i<s1;++i)
		tempo1.push_back(num.at(i));

	//tempo2=number.getNum();
	for(int i=0;i<s2;++i)
		tempo2.push_back(number.getNum().at(i));

	//the below is doing e.g. 126 +12345
	//126 =>621
	//12345 =>54321
	//6+5,2+4,1+3,2,1
	//11 6 4 2 1 
	//1 7 4 2 1
	//reverse => 1 2 4 7 1
	reverse(tempo1.begin(),tempo1.end());
	reverse(tempo2.begin(),tempo2.end());
	if(s1>s2)
	{
		for(int i=0;i<s2;++i)
		{
			int addNo=tempo1.at(i)+tempo2.at(i);
			addR.push_back(addNo);
		}
		for (int i=s2;i<s1;++i)
		{
			int addNo=tempo1.at(i);
			addR.push_back(addNo);
		}
	}
	else if (s1<s2)
	{
		 for(int i=0;i<s1;++i)
		 {
		 	int addNo=tempo1.at(i)+tempo2.at(i);
			addR.push_back(addNo);
		 }
	     for (int i=s1;i<s2;++i)
	     {
	     	int addNo=tempo2.at(i);
		 	addR.push_back(addNo);
		 }
		 
	}
	else if(s1==s2)
	{
		for(int i=0;i<s1;++i)
		{
			int addNo=tempo1.at(i)+tempo2.at(i);
			addR.push_back(addNo);
		}
	}
	int s3=addR.size();
	for(int i=0;i<s3;++i)
	{
		if(addR.at(i)>9)
		{
			if(i!=s3-1)
			{
				int tmp=addR.at(i);
				addR.at(i)=tmp%10;
				addR.at(i+1)+=tmp/10;
			}
			else
			{
				int tmp=addR.at(i);
				addR.at(i)=tmp%10;
				int plus=tmp/10;
				addR.push_back(plus);
			}
		}
	}
	reverse(addR.begin(),addR.end());
	add.setInt(addR);
	return add;
}

HugeInt HugeInt::operator-(HugeInt& number)
{
	HugeInt dif;
	vector <int> tempo1,tempo2,difR;
	bool bs=true;
	int s1=num.size();
	int s2=number.getNum().size();
	
	//tempo1=num;
	for(int i=0;i<s1;++i)
		tempo1.push_back(num.at(i));
	
	//tempo2=number.getNum();
	for(int i=0;i<s2;++i)
		tempo2.push_back(number.getNum().at(i));
	
	//it acts similar to +, e.g. 9781-856
	//9781 =>1 8 7 9
	//456 => 6 5 8
	//1-6, 8-5, 7-8, 9
	//-5 3 -1 9
	//-5+10 3-1 -1+10 9-1
	//5 2 9 8
	//reverse=> 8925
	reverse(tempo1.begin(),tempo1.end());
	reverse(tempo2.begin(),tempo2.end());
	if(s1>s2)
	{
		for(int i=0;i<s2;++i)
		{
			int minus = tempo1.at(i)-tempo2.at(i);
			difR.push_back(minus);
		}
		for(int i=s2;i<s1;++i)
		{
			int minus=tempo1.at(i);
			difR.push_back(minus);
		}
	}
	else if(s1<s2)
	{
		bs=false;
		for(int i=0;i<s1;++i)
		{
			int minus = tempo2.at(i)-tempo1.at(i);
			difR.push_back(minus);
		}
		for(int i=s1;i<s2;++i)
		{
			int minus =tempo2.at(i);
			difR.push_back(minus);
		}
	}
	else if(s1==s2)
	{
		for(int i=s1-1;i>=0;--i)
		{
			if(tempo1.at(i)>tempo2.at(i))
				break;	
			else if(tempo1.at(i)<tempo2.at(i))
			{
				bs=false;
				break;
			}
			else if(tempo1.at(i)==tempo2.at(i))
			{
				if(i==0)
				{
					difR.push_back(0);
					dif.setInt(difR);
					return dif;
				}
			}
		}
		for(int i=0;i<s1;++i)
		{
			int minus = tempo1.at(i) - tempo2.at(i);
			difR.push_back(minus);
		}
	}
	int s3=difR.size();
	for(int i=0;i<s3;++i)
	{
		if(difR.at(i)<0)
		{
			difR.at(i)+=10;
			--difR.at(i+1);
		}
	}
	if(difR.at(s3-1)==0&&s3!=1)
		difR.pop_back();
	int s4=difR.size();
	if(bs==false)
		difR.at(s4-1)=-difR.at(s4-1);
	reverse(difR.begin(),difR.end());
	dif.setInt(difR);
	return dif;
	/*int diff;
	diff= num - number.getNum();
	return diff;*/
}

void HugeInt::operator=(const HugeInt& number)
{
	//same as setInt(), save the value to num by push_back()
	num.clear();
	int siz=number.getNum().size();
	for(int i=0;i<siz;++i)
		num.push_back(number.getNum().at(i));
}



