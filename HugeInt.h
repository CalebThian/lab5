#include <iostream>
#include <vector>
#include <string>
using namespace std;

class HugeInt
{
	public:
		HugeInt(int a);
		HugeInt();
		HugeInt(string b);
		vector <int>& getNum();
		vector <int> getNum() const;
		void setInt(vector <int> s);
		friend ostream& operator<<(ostream&,HugeInt&);
		friend istream& operator>>(istream&,HugeInt&);
		HugeInt operator+(HugeInt&);
		HugeInt operator-(HugeInt&);
		void operator=(const HugeInt&);
		
	private:
		vector <int> num;
};

